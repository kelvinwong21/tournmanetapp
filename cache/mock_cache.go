package cache

import (
	"encoding/json"
	"github.com/kwong21/ryuji-ng/models"
	log "github.com/sirupsen/logrus"
)

var cacheMap map[string][]byte

func (MockCache) StoreSession(token string, serializedStuct []byte) error {
	cacheMap = make(map[string][]byte)

	log.WithFields(log.Fields{
		"Log":  "Unit Testing",
		"Info": "Adding serialized struct to mock cache",
	}).Debug()

	cacheMap[token] = serializedStuct

	return nil
}

func (MockCache) RetrieveProfile(token string) *models.Account {
	account := new(models.Account)

	json.Unmarshal(cacheMap[token], &account)

	log.WithFields(log.Fields{
		"Log":  "Unit Testing",
		"Info": "Retreiving struct to mock cache",
	}).Debug()

	return account
}
