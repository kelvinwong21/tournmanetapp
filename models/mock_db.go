package models

import (
	log "github.com/sirupsen/logrus"
	"gopkg.in/DATA-DOG/go-sqlmock.v1"
	"errors"
)

func (MockDB) CreateProfile(p *Profile, accountID int64) error {
	return nil
}

func (MockDB) GetProfile(accountID int64) (*Profile, error) {
	return nil, nil
}
func (MockDB) GetAccount(email string) (*Account, error) {
	return nil, nil
}

func (MockDB) CreateAccount(a *Account) (error, int64) {

	if (a.Profile.GivenName == "Server" && a.Profile.Surname == "Error") {
		return errors.New("Error"), 0
	}
	db, mock, err := sqlmock.New()
	mock.ExpectPrepare("INSERT INTO Account").ExpectExec().WithArgs(a.Email, a.Password, a.IsAdmin).
		WillReturnResult(sqlmock.NewResult(1, 1))
	if err != nil {
		log.Fatalf("Error creating a stub db connection: %s", err)
	}
	defer db.Close()

	stmt, err := db.Prepare(`INSERT INTO Account(Email, PasswordHash, IsAdmin)
									VALUES(?, ?, ?)`)

	if err != nil {
		log.Fatalf("Error executing stub db statment: %s", err)
	}


	res, err := stmt.Exec(a.Email, a.Password, false)
	if err != nil {
		log.Fatalf("Error executing stub db statement: %s", err)
	}

	id, err := res.LastInsertId()


	return err, id
}
