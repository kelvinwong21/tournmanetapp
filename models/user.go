package models

import (
	"fmt"
	"github.com/go-sql-driver/mysql"
	"github.com/kwong21/ryuji-ng/validate"
	log "github.com/sirupsen/logrus"
	"golang.org/x/crypto/bcrypt"
)

type Account struct {
	Id       int64 `schmea:"-"`
	Email    string
	Password []byte `schema:"-"`
	Profile  *Profile
	IsAdmin  bool `schema:"-"`
}

type Profile struct {
	GivenName   string
	Surname     string
	Team        string
	Instructor  string
	DateOfBirth string
}

func (a *Account) SetHashedPassword(p string) error {

	if !validate.CheckIfNotEmpty(p) {
		a.Password = make([]byte, 0)
		return nil
	}

	hash, err := bcrypt.GenerateFromPassword([]byte(p), bcrypt.DefaultCost)

	if err != nil {
		log.WithFields(log.Fields{
			"Log":   "password hashing",
			"user":  a.Email,
			"Error": err,
		}).Error("Password hashing failed, aborting creating the user")
		return err
	}

	a.Password = hash
	return nil
}

func (a *Account) CheckHashedPassword(p string) bool {

	return validate.CheckIfHashedPasswordIsFromString(a.Password, p)
}

func (db *DB) CreateAccount(a *Account) (error, int64) {

	stmt, err := db.Prepare(`INSERT INTO Account(Email, PasswordHash, IsAdmin)
									VALUES(?, ?, ?)`)
	defer stmt.Close()

	if err != nil {
		logDatabaseErrors(err, "Error creating prepared statement.")
		return err, -1
	}

	res, err := stmt.Exec(a.Email, a.Password, false)

	if err != nil {
		logDatabaseErrors(err, "Error running prepared statement.")
		return err, -1
	}

	lastId, err := res.LastInsertId()

	if err != nil {
		logDatabaseErrors(err, "Error retrieving last insert ID.")
	}

	log.WithFields(log.Fields{
		"Log":       "DB",
		"user":      a.Email,
		"AccountID": lastId,
	}).Info("Inserted into the Account table")

	return nil, lastId
}

func CheckIfDuplicateEmailError(err error) bool {
	mysqlErr, ok := err.(*mysql.MySQLError)
	if ok && mysqlErr.Number == 1062 {
		return true
	}
	return false
}

func (db *DB) CreateProfile(p *Profile, accountID int64) error {

	stmt, err := db.Prepare(`INSERT INTO Profile (AccountId, GivenName, Surname, Team, Instructor, DateOfBirth)
									VALUES (?, ?, ?, ?, ?, ?)`)
	defer stmt.Close()

	if err != nil {
		logDatabaseErrors(err, "Error creating prepared statement.")
		return err
	}

	res, err := stmt.Exec(accountID, p.GivenName, p.Surname, p.Team, p.Instructor, p.DateOfBirth)

	if err != nil {
		logDatabaseErrors(err, "Error running prepared statement.")
		return err
	}

	lastId, err := res.LastInsertId()

	if err != nil {
		logDatabaseErrors(err, "Error retrieving last insert ID.")
	}

	log.WithFields(log.Fields{
		"Log":        "DB",
		"First Name": p.GivenName,
		"Last Name":  p.Surname,
		"AccountID":  lastId,
	}).Info("Inserted into the Profile table")

	return nil

}

func (db *DB) GetAccount(email string) (*Account, error) {
	a := new(Account)
	// set a dummy value for account
	a.Id = int64(-1)
	err := db.QueryRow(`SELECT Id, Email, PasswordHash, IsAdmin
 						       FROM Account WHERE Email = ?`, email).Scan(&a.Id, &a.Email, &a.Password, &a.IsAdmin)

	if err != nil {
		logDatabaseErrors(err, fmt.Sprintf("Error querying for user Account with email %s", email))
	}

	return a, err
}

func (db *DB) GetProfile(accountID int64) (*Profile, error) {
	p := new(Profile)

	err := db.QueryRow(`SELECT GivenName, Surname, Team, Instructor, DateOfBirth
								FROM Profile WHERE AccountId = ?`, accountID).Scan(&p.GivenName, &p.Surname, &p.Team, &p.Instructor, &p.DateOfBirth)

	if err != nil {
		logDatabaseErrors(err, fmt.Sprintf("Error querying for user Profile with AccountID %v", accountID))
	}

	return p, err
}

func ValidateFormEntries(u *Account, email string, password string) []string {
	errMessages := make([]string, 0)

	if !validate.CheckIfAlphaCharacter(u.Profile.GivenName) || !validate.CheckIfNotEmpty(u.Profile.GivenName) {
		errMessages = append(errMessages, "Please provide your given name.")
	}

	if !validate.CheckIfAlphaCharacter(u.Profile.Surname) || !validate.CheckIfNotEmpty(u.Profile.Surname) {
		errMessages = append(errMessages, "Please provide your last name.")
	}

	if !validate.CheckIfValidEmail(u.Email) || !validate.CheckIfNotEmpty(u.Email) || !validate.CheckIfInputIsConfirmed(u.Email, email) {
		errMessages = append(errMessages, "Please provide a valid email.")
	}

	if len(u.Password) <= 0 {
		errMessages = append(errMessages, "Please provide a password for your account.")
	}

	if !validate.CheckIfHashedPasswordIsFromString(u.Password, password) {
		errMessages = append(errMessages, "Please verify the passwords entered matches.")
	}

	if !validate.CheckIfDate(u.Profile.DateOfBirth) || !validate.CheckIfNotEmpty(u.Profile.DateOfBirth) {
		errMessages = append(errMessages, "Please provide a valid date of birth (YYYY-MM-DD).")
	}

	return errMessages
}
