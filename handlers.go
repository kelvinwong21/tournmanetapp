package main

import (
	"fmt"
	"github.com/gorilla/mux"
	"github.com/kwong21/ryuji-ng/models"
	log "github.com/sirupsen/logrus"
	"net/http"
	"os"
)

func routeErrorPages(w http.ResponseWriter, errorCode int, errorType string) {

	w.WriteHeader(errorCode)
	htmlPage := fmt.Sprintf("errors/%s", errorType)

	generateHTML(w, nil, "templates/tmpl_layout_pub", htmlPage)
}

func routeHomePage(w http.ResponseWriter, r *http.Request) {
	generateHTML(w, nil, "templates/tmpl_layout_pub", "public/index")
}

func routeUserLogout(w http.ResponseWriter, r *http.Request) {
	err := expireSession(r, w)

	if err != nil {
		routeErrorPages(w, http.StatusInternalServerError, "server_error")
		return
	}

	generateHTML(w, nil, "templates/tmpl_layout_pub", "public/logged_out")
}
func routePublicPages(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	htmlPage := fmt.Sprintf("public/%s", vars["page"])

	if _, err := os.Stat("views/" + htmlPage + ".html"); err == nil {
		generateHTML(w, nil, "templates/tmpl_layout_pub", htmlPage)
	} else {
		routeErrorPages(w, http.StatusNotFound, "not_found")
	}
}

func routeUserSignUp(w http.ResponseWriter, r *http.Request) {
	err := r.ParseForm()
	a := new(models.Account)
	err = decode.Decode(a, r.PostForm)
	err = a.SetHashedPassword(r.FormValue("Password"))

	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		generateHTML(w, []string{fmt.Sprint("Error creating account")}, "templates/tmpl_layout_pub", "public/sign-up")
		return
	}

	validationErrors := models.ValidateFormEntries(a, r.Form.Get("EmailConfirm"), r.Form.Get("PasswordConfirm"))

	if len(validationErrors) > 0 {
		w.WriteHeader(http.StatusBadRequest)
		generateHTML(w, validationErrors, "templates/tmpl_layout_pub", "public/sign-up")
		return
	}

	err, lastInsertID := env.db.CreateAccount(a)

	if err != nil {
		if models.CheckIfDuplicateEmailError(err) {
			w.WriteHeader(http.StatusBadRequest)
			generateHTML(w, []string{fmt.Sprintf("An account with e-mail %s already exists", a.Email)},
				"templates/tmpl_layout_pub", "public/sign-up")
			return
		} else {
			routeErrorPages(w, http.StatusInternalServerError, "server_error")
			return
		}
	}

	err = env.db.CreateProfile(a.Profile, lastInsertID)

	if err != nil {
		if models.CheckIfDuplicateEmailError(err) {
			w.WriteHeader(http.StatusBadRequest)
			generateHTML(w, []string{fmt.Sprintf("An account with e-mail %s already exists", a.Email)},
				"templates/tmpl_layout_pub", "public/sign-up")
			return
		} else {
			routeErrorPages(w, http.StatusInternalServerError, "server_error")
			return
		}
	}

	err = createSessionForAuthenticatedUser(w, r, a)

	if err != nil {
		routeErrorPages(w, http.StatusInternalServerError, "server_error")
		return
	}

	http.Redirect(w, r, "/user/overview", 200)
}

func routeUserLogin(w http.ResponseWriter, r *http.Request) {
	err := r.ParseForm()

	if err != nil {
		log.WithFields(log.Fields{
			"Log":   "user login",
			"Error": err,
		}).Error("Error parsing login form")
		w.WriteHeader(http.StatusBadRequest)
		generateHTML(w, "Error logging In", "templates/tmpl_layout_pub", "public/login")
		return
	}

	authed, a := authenticateUser(r.Form.Get("email"), r.Form.Get("password"))

	if !authed {
		w.WriteHeader(http.StatusBadRequest)
		generateHTML(w, "Invalid email and/or password.", "templates/tmpl_layout_pub", "public/login")
		return
	}

	err = createSessionForAuthenticatedUser(w, r, a)

	if err != nil {
		routeErrorPages(w, http.StatusInternalServerError, "server_error")
		return
	}
	http.Redirect(w, r, "/user/overview", 302)
}

func routeUserOverview(w http.ResponseWriter, r *http.Request) {
	generateHTML(w, nil, "templates/tmpl_layout_user", "user/overview")
}
