package validate

import (
	"regexp"
	"strings"
	"time"
	"strconv"
	"golang.org/x/crypto/bcrypt"
)

var dateTemplate = "2006-01-02"

// CheckIfValidEmail checks if the passed email parameter is of proper email format
// Proper email is in the form of username@domain.com
func CheckIfValidEmail(email string) (bool){
	match, _ := regexp.MatchString(`^[\w-\.]+@([\w-]+\.)+[\w-]{2,4}$`, email)
	if match{
		return true
	}else{
		return false
	}
}

// CheckIfAlphaNumeric checks if the passed parameter contains only alphanumeric characeters
// '-' and '_' are also accepted.
func CheckIfAlphaNumeric(alphaNum string) (bool) {
	match, _ := regexp.MatchString("^[A-Za-z0-9\\s]*$", alphaNum)
	if match {
		return true
	} else {
		return false
	}
}

// CheckIfNumeric checks if passed parameter is an int
func CheckIfNumeric(text string) (bool){
	if _, err := strconv.Atoi(text); err != nil {
		return false
	} else {
		return true
	}
}

// CheckIfAlphaCharacter checks if the passed parameter contains only alpha characeters
func CheckIfAlphaCharacter(text string) (bool){
	match, _ := regexp.MatchString("^[a-zA-Z\\s]*$", text)
	if match{
		return true
	} else {
		return false
	}
}

// CheckIfInputIsConfirmed checks if the two values are the same.
func CheckIfInputIsConfirmed(text, textRepeat string)(bool){
	if text == textRepeat {
		return true
	} else {
		return false
	}
}

// CheckIfValidDateOfBirth checks if a date formated value is provided
// YYYY-MM-DD
func CheckIfValidDateOfBirth(text string) (bool){
	match, _ := regexp.MatchString(`^\d{4}-\d{2}-\d{2}$`, text)
	if match {
		return true
	} else {
		return false
	}
}

func CheckIfNotEmpty(text string) (bool){
	result := strings.Replace(text, " ", "", -1)
	if result == "" {
		return false
	} else {
		return true
	}
}

// CheckIfDate checks if the date provided is a real date
func CheckIfDate(d string) (bool) {
	_, err := time.Parse(dateTemplate, d)

	if err != nil {
		return false
	} else {
		return true
	}
}

// CheckDateOrder checks if date a is before date b
func CheckDateOrder( f string, l string) (bool) {
	fd, _ := time.Parse(dateTemplate, f)
	ld, _ := time.Parse(dateTemplate, l)

	if fd.Before(ld){
		return true
	} else {
		return false
	}
}

func CheckIfHashedPasswordIsFromString(hash []byte, p string,) (bool) {

	if err := bcrypt.CompareHashAndPassword(hash, []byte(p)); err != nil {
		return false
	} else {
		return true
	}
}